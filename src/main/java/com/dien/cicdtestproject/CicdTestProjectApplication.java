package com.dien.cicdtestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CicdTestProjectApplication {

	@RequestMapping("/hello")
	public String home() {
		return "hello world 123";
	}

	public static void main(String[] args) {
		SpringApplication.run(CicdTestProjectApplication.class, args);
	}

}
