# MUSIC DB
description

## Table of Contents
- [Project structure](#markdown-header-project-structure)
- [Start application](#markdown-header-start-application)
- [CICD Flow](#markdown-header-cicd-flow)
- [Docs](#markdown-header-docs)

### Project structure
```
.
├── Dockerfile
├── README.md
├── bitbucket-pipelines.yml 
├── build.gradle
├── dev.properties
├── gradle/
├── gradle.properties
├── gradlew
├── gradlew.bat
├── lombok.config
├── pipeline-scripts/
├── settings.gradle
└── src/
    ├── main/
    │   ├── java/
    │   │   └── com/
    │   │       └── amanotes/
    │   │           └── acm/
    │   │               ├── Application.java
    │   │               ├── ...
    │   └── resources/
    └── test/
        ├── java/
        │   └── com/
        │       └── amanotes/
        │           └── acm/
        │               ├── api/
        │               └── fitnessfunction/
        └── resources/
```

* `build.gradle` Gradle build script. You can look up project plugins and dependencies here
* `bitbucket-pipelines.yml` CI/CD Pipeline. You can read [CI/CD flow](#markdown-header-cicd-flow) for a full reference.
* `pipeline-scripts` Folder contains version file and bash scripts used in `bitbucket-pipelines.yml`
* `test` Directory with test packages:
    * `api` Api unit test
    * `fitnessfunction` Project ArchUnit test

### Start application
#### Prerequisites
* Java 11
* Gradle (check version)
* Postgres 15.6 (check version later)

Make sure that you properly connected to DB (Find DB credentials in `application.properties`)
#### Run Application
```
gradlew bootRun
```
The output should be similar to the following:
<image>

View swagger api: localhost:8080/swagger-ui.html
#### Run Test
```
gradlew test
```
### CICD Flow

### Docs